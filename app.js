const path = require('path');
const express = require('express');
const morgan = require('morgan');
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const hpp = require('hpp');
const hbs = require('hbs');
const cookieParser = require('cookie-parser');

const multer = require('multer');
const cors = require('cors');
const fs = require('fs')
const Loki = require('lokijs');
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});


// init upload
const upload = multer({
    storage: storage
}).single('images');

/////////////// my exports and imports //////////

const productRouter = require('./routes/productRoutes');
const adminRouter = require('./routes/adminRoutes');

const userRouter = require('./routes/userRoutes');
const reviewRouter = require('./routes/reviewRoutes');
const viewRouter = require('./routes/viewRoutes');
const AppError = require('./utils/appError');
const globalErrorHandler = require('./controllers/errorContoller');

const app = express();

// VIEW engine setup

app.set("views", path.join(__dirname, "views"));
//app.set('views', path.join(__dirname, '/views/layout/users'))
//app.set('views', [__dirname + '/views', __dirname + '/views/layout/product', __dirname + '/views/layout/users'])
app.set('view engine', 'hbs');

//hsb helpers and partials
hbs.registerHelper('toLowerCase', function (str) {
    return str.toLowerCase();
});

hbs.registerHelper('toLocaleString', function (dateObject) {
    const data = dateObject.toLocaleString('en-us',
        {
            month: 'long', year: 'numeric'
        });
    return data;
});
hbs.registerHelper('times', function(n, block) {
    var accum = '';
    for(var i = 0; i < n; ++i)
        accum += block.fn(i);
    return accum;
});

hbs.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('split', function(title) {

    var t = title.split(' ');
    // return [t[0], t[1]];
    return t[0];
});

hbs.registerPartials(__dirname + '/views/partials/');



// 1) GLOBAL middleware
// serving static files
app.use(express.static(path.join(__dirname, '/public/')));

console.log(__dirname)

app.use(express.urlencoded({extended: false}));
app.use(cookieParser());

// set Security HTTP headers
app.use(helmet());

if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

//allows 100 req per hour, limit requests from same IP
const limiter = rateLimit({
    max: 100,
    windowMs: 60 * 60 * 1000,
    message: 'Too many requests from this IP, please try again in an hour!'
});

app.use('/api', limiter);

/// this middleware helps us to parse json
app.use(express.json({limit: '10kb'}));

// Data sanitization against noSQL query injection
app.use(mongoSanitize());

// Data sanitization again XSS
app.use(xss());

// Prevent parameter pollution
app.use(hpp({
    whitelist: [
        'duration',
        'ratingsQuantity',
        'ratingsAverage',
        'maxGroupSize',
        'difficulty',
        'price'
    ]
}));


// Test middleware
app.use((req, res, next) => {
    req.requestTime = new Date().toISOString();
    console.log(req.cookies);
    next();
});



// view routes
app.use('/', viewRouter);

//routes
app.use('/api/v1/products', productRouter);

//app.use('/api/v1/admin', adminRouter);
app.use('/api/v1/admin', adminRouter);
app.use('/api/v1/users', userRouter);
app.use('/api/v1/reviews', reviewRouter);




app.all('*', (req, res, next) => {
// const err = new Error(`Can not find ${req.originalUrl} on this server!`);
    // err.status = 'failed';
    // err.statusCode = 404;
    next(new AppError(`Can not find ${req.originalUrl} on this server!`, 404));
});

// ERROR handling middleware
app.use(globalErrorHandler);

module.exports = app;

const Category = require('../../models/CategoryModel');
const Product = require('../../models/ProductModel');
const Tour = require('../../models/tourModel');
const catchAsync = require('../../utils/catchAsync');
const umiRequest = require('umi-request');
const mongoose = require('mongoose');
const handlerFactory = require('../handlerFactory');

const multer = require('multer');
const multerFunctions = require('../../middleware/uploadFiles');


const upload = multer({
    storage: multerFunctions.multerStorage,
    fileFilter: multerFunctions.multerFilter
});

exports.uploadImage = upload.fields([
    {name: 'imageCover', maxCount: 1},
    {name: 'images', maxCount: 20}
]);

exports.resizeImages = catchAsync(async (req, res, next) => {
    if(!req.files.imageCover || !req.files.images){
        return next();
    }
    // Cover image
    req.body.imageCover = `product-${req.params.id}-${Date.now()}-cover.jpeg`;
    await sharp(req.files.imageCover[0].buffer)
        .resize(2000, 1333)
        .toFormat('jpeg')
        .jpeg({ quality: 90 })
        .toFile(`public/img/products/${req.body.imageCover}`);

    // Images
    req.body.images = [];
    await Promise.all(
        req.files.images.map(async (file, i) => {
            const filename = `product-${req.params.id}-${Date.now()}-${i + 1}.jpeg`;

            await sharp(file.buffer)
                .resize(2000, 1333)
                .toFormat('jpeg')
                .jpeg({ quality: 90 })
                .toFile(`public/img/products/${filename}`);

            req.body.images.push(filename);
        })
    );

    next();
})




exports.getAddCategoryPage = catchAsync(async (req, res, next) => {

    const categories = await Category.find({soft_delete: 0});


    res.status(200).render('admin/admin_add_category', {
        title: "Add category",
        category: categories,
        success: "Category Added Succesfully"
    })
})

exports.getAddProductPage = catchAsync(async (req, res, next) => {

    const category = await Category.find({soft_delete: 0});

    const newProduct = await Product.find().populate('category', 'name -_id')


    res.status(200).render('admin/admin_add_product', {
        title: "Add product",
        category: category,
        product: newProduct
    })
});

exports.AddProduct = catchAsync(async (req, res, next) => {
    console.log(req.body)
    const newProduct = await  Product.create(req.body)


    res.status(201).json({
        status: "Success",
        data: {
            product: newProduct
        }
    });
})

exports.AddCategory = catchAsync(async (req, res, next) => {

    const newCategory = await  Category.create(req.body)

    res.status(201).json({
        status: "Success",
        data: {
            product: newCategory
        }
    });
})



exports.deleteProducts = handlerFactory.deleteOne(Product);
exports.deleteCategory = handlerFactory.deleteOne(Category);
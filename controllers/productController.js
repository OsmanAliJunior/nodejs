const Tour = require('../models/tourModel');
const APIFeatures = require('../utils/apiFeatures');
const catchAsync = require('./../utils/catchAsync');
const AppError = require('../utils/appError');
const handlerFactory = require('./handlerFactory');
const Category = require('../models/CategoryModel');
const Product = require('../models/ProductModel');

exports.aliasTopTours = async (req, res, next) => {
    req.query.limit = '5';
    req.query.sort = '-ratingsAverage,price';
    req.query.fields = 'name,price,ratingsAverage,summary,difficulty';
    next();
};


exports.getTour = handlerFactory.getOne(Tour, {
    path: 'reviews'
});
// exports.getAllProducts = handlerFactory.getAll(Tour);
//exports.createTour = handlerFactory.createOne(Tour);
// exports.createTour = handlerFactory.createOne(Tour);
// exports.updateTour = handlerFactory.updateOne(Tour);
// exports.deleteTour = handlerFactory.deleteOne(Tour);
// exports.deleteCategory = handlerFactory.deleteOne(Category);
// exports.deleteProducts = handlerFactory.deleteOne(Product);



// exports.getAllTours = async (req, res, next) => {
//     try {
//
//         // EXECUTE QUERY
//         const features = new APIFeatures(Tour.find(), req.query)
//             .filter()
//             .sort()
//             .limitFields()
//             .paginate();
//         const tours = await features.query;
//
//
//         res.status(200).json({
//             status: "Success",
//             results: tours.length,
//             data: {
//                 tours
//             }
//         })
//     } catch (e) {
//         res.status(400).json({
//             status: "Failed",
//             message: e.message
//         })
//     }
// };






// exports.getTour = catchAsync(async (req, res, next) => {
//     const tour = await Tour.findById(req.params.id).populate('reviews');
//     if (!tour) {
//         return next(new AppError(`No tour found with that iD`, 404));
//     }
//
//     res.status(200).json({
//         status: "Success",
//         data: {
//             tour: {
//                 tour
//             }
//         }
//     });
// });

// exports.updateTour = async (req, res) => {
//     try {
//         const tour = await Tour.findByIdAndUpdate(req.params.id, req.body, {
//             new: true,
//             runValidators: true
//         });
//
//         res.status(200).json({
//             status: "Success",
//             data: {
//                 tour: tour
//             }
//         });
//     } catch (e) {
//         res.status(400).json({
//             status: "Failed",
//             message: e
//         });
//     }
//
// };

// exports.deleteTour = async (req, res) => {
// //     try {
// //         const tour = await Tour.findByIdAndDelete(req.params.id, () => {
// //             console.log("ejenizin amy");
// //         });
// //
// //         res.status(204).json({
// //             status: "Success",
// //             data: {
// //                 tour
// //             }
// //         })
// //     } catch (e) {
// //         res.status(400).json({
// //             status: "Failed",
// //             message: e
// //         })
// //     }
// // };

// exports.createTour = catchAsync(async (req, res) => {
//
//     const newTour = await Tour.create(req.body);
//
//     res.status(201).json({
//         status: "Success",
//         data: {
//             tour: newTour
//         }
//     });
//
//
// });



// CALCULATE statistics
exports.getTourStats = async (req, res, next) => {
    try {
        const stats = await Tour.aggregate([
            {
                $match: {ratingsAverage: {$gte: 4.5}}
            },
            {
                $group: {
                    // _id: '$difficulty',
                    _id: {$toUpper: '$difficulty'},
                    numTours: {$sum: 1},
                    numRatings: {$sum: '$ratingsQuantity'},
                    avgRating: {$avg: '$ratingsAverage'},
                    avgPrice: {$avg: '$price'},
                    minPrice: {$min: '$price'},
                    maxPrice: {$min: '$price'},
                }
            },
            {
                $sort: {
                    avgPrice: 1
                }
            },
            // {
            //     $match: {
            //         _id: {$ne: 'EASY'}
            //     }
            // }
        ]);
        res.status(200).json({
            status: 'Success',
            data: {
                stats
            }
        });
    } catch (e) {
        res.status(400).json({
            status: "Failed",
            message: e
        })
    }
};

exports.getMonthlyPlan = async (req, res) => {
    try {
        const year = req.params.year * 1;
        const plan = await Tour.aggregate([
            {
                $unwind: '$startDates'
            },
            {
                $match: {
                    startDates: {
                        $gte: new Date(`${year}-01-01`),
                        $lte: new Date(`${year}-12-31`),
                    }
                }
            },
            {
                $group: {
                    _id: {$month: '$startDates'},
                    numTourStarts: {$sum: 1},
                    tours: {$push: '$name'}
                }
            },
            {
                $addFields: {month: '$_id'},
            },
            {
                $project: {_id: 0}
            },
            {
                $sort: {numberTourStarts: -1}
            },
            {
                $limit: 12
            }
        ]);

        res.status(200).json({
            status: 'Success',
            data: {
                plan
            }
        })
    } catch (e) {
        res.status(400).json({
            status: "Failed",
            message: e
        })
    }
};


// ANOTHER VARIANT FOR getAllTours function
//  console.log(req.query);
// BUILD QUERY
// 1A) Filtering
// const queryObj = {...req.query};
// const excludedFields = ['page', 'sort', 'limit', 'fields'];
// excludedFields.forEach(el => delete queryObj[el]);
//
// // 1B) Advanced filtering
//
// //an earlier version was problem with $ sign. new version of express handles
// //this issue automatically
//
// // {difficulty: 'easy', duration: {$gte: 5}}
//
// let query = Tour.find(queryObj);


// 2) Sorting
//         if (req.query.sort) {
//             const sortBy = req.query.sort.split(',').join(' ');
//             console.log(sortBy);
//             query = query.sort(sortBy);
// //sort('price ratingsAverage:')
//         } else {
//             query = query.sort('-createdAt');
//         }

// 3) Field limiting
// if (req.query.fields) {
//     const fields = req.query.fields.split(',').join(' ');
//     query = query.select(fields)
// } else {
//     query = query.select('-__v');
// }

// 4) Pagination
//page=2&limit=10 => results 1-10 on page1 and 11-20 on page 2
//how many results we've to skip, to get on page2 (answer is 10)
//code below converts string to number and gets default page
// const page = req.query.page * 1 || 1;
// const limit = req.query.limit * 1 || 100;
// const skip = (page - 1) * limit;
// console.log(req.query)
// query = query.skip(skip).limit(limit);
//
// if (req.query.page) {
//     const numberOfTours = await Tour.countDocuments();
//     console.log(numberOfTours); // 9
//     if (skip >= numberOfTours) throw  new Error('This page does not exits!');
// }

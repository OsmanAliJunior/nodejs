const Review = require('../models/Review');
const APIFeatures = require('../utils/apiFeatures');
const catchAsync = require('./../utils/catchAsync');
const AppError = require('../utils/appError');
const handlerFactory = require('./handlerFactory');


// POST /tour/34343/reviews
// GET /tour/34343/reviews



exports.getAllReview = handlerFactory.getAll(Review);
exports.getReview = handlerFactory.getOne(Review);
exports.createReview = handlerFactory.createOne(Review);
exports.deleteReview = handlerFactory.deleteOne(Review);
exports.updateReview = handlerFactory.updateOne(Review);

exports.setTourUserIdx = (req, rex, next) => {
    // allow nested routes
    if (!req.body.tour) req.body.tour = req.params.tourId;
    if (!req.body.user) req.body.user = req.user._id;
    next();
};









// exports.getAllReview = async (req, res, next) => {
//
//
//     let filter = {};
//     try {
//
//         if (req.params.id) {
//             filter = {tour: req.params.id}
//         }
//         // EXECUTE QUERY
//         const reviews = await Review.find(filter);
//
//
//         res.status(200).json({
//             status: "Success",
//             results: reviews.length,
//             data: {
//                 reviews: reviews
//             }
//         })
//     } catch (e) {
//         console.log(e.message);
//         res.status(400).json({
//             status: "Failed",
//             message: e.message
//         })
//     }
// };




// exports.creatReview = async (req, res, next) => {
//     try {
//
//         // allow nested routes
//         if (!req.body.tour) req.body.tour = req.params.tourId;
//         if (!req.body.user) req.body.user = req.user._id;
//
//
//         const review = await Review.create(req.body);
//
//         res.status(200).json({
//             status: "Success",
//             data: {
//                 reviews: review
//             }
//         })
//     } catch (e) {
//         console.log(e.message);
//         res.status(400).json({
//             status: 'Failed',
//             message: e.message
//         })
//     }
// };



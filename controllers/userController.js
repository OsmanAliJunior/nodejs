const User = require('../models/User');
const AppError = require('../utils/appError');
const catchAsync = require('./../utils/catchAsync');
const handlerFactory = require('./handlerFactory');


const filterObj = (obj, ...allowedFields) => {
    const newObj = {};
    Object.keys(obj).forEach(el => {
        if (allowedFields.includes(el)) {
            newObj[el] = obj[el];
        }
    });
    return newObj;
};

//
// exports.getAllUsers = async (req, res, next) => {
//     try {
//         const users = await User.find();
//
//         res.status(200).json({
//             status: "Success",
//             results: users.length,
//             data: {
//                 users: users
//             }
//         })
//     } catch (e) {
//         res.status(400).json({
//             status: "Failed",
//             message: e.message
//         })
//     }
// };

exports.updateMe = async (req, res, next) => {
    // 1) Create Error if user POSTs password Data
    if (req.body.password || req.body.passwordConfirm) {
        return next(new AppError(
            "This route is not for password updates! Please use /updateMyPassword"
            ),
            400)
    }

    // 2) Filtered out unwanted fieldNames
    const filteredBody = filterObj(req.body, 'name', 'email');
   // 3) Update user
     const updatedUser = await User.findByIdAndUpdate(req.user.id, filteredBody, {
        new: true, runValidators: true
    });

    res.status(200).json({
        status: 'Success',
        data: {
            user: updatedUser
        }
    });
};

exports.getMe = (req, res, next) => {
    req.params.id = req.user.id;
    next();
}

exports.deleteMe = catchAsync(async (req, res, next) => {
   await User.findByIdAndUpdate(req.user._id, {active: false});

   res.status(204).json({
      status: 'Success',
      data: null
   });
});


exports.getAllUsers = handlerFactory.getAll(User);
exports.getUser = handlerFactory.getOne(User);
exports.deleteUser = handlerFactory.deleteOne(User);
exports.updateUser = handlerFactory.updateOne(User);



// exports.getUser = (req, res) => {
//     res.status(500).json({
//         status: 'error',
//         message: 'This route is not defined yet'
//     });
// };



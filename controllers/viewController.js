const Category = require('../models/CategoryModel');
const Product = require('../models/ProductModel');
const Tour = require('../models/tourModel');
const catchAsync = require('../utils/catchAsync');
const axios = require('axios');
const umiRequest = require('umi-request');


exports.getIndexPage = catchAsync(async (req, res) => {
    // 1) Get tour data from collection
         const products = await Tour.find();

    //console.log(products);
    //bc751ee5355bb87777c7e23ee1db00a5323bac9a
    // 2) build template
    //3) render that template using tour data from 1
    res.status(200).render('index', {
        title: "minastyle",
        products: products
    })
});



exports.getTour = catchAsync(async (req, res, next) => {

    // 1) Get the data, for the requested tour (including reviews and guides)
    const tour = await Tour.findOne({slug: req.params.slug}).populate({
        path: 'reviews guides',
        fields: 'review rating user name'
    });
    // 2) Build template
 // 3) Render template using data from 1
    console.log(tour.reviews);
    res.status(200).render('tour', {
        title: "Details",
        tour: tour,
        reviews: tour.reviews

    })
});

exports.getLoginForm = catchAsync(async (req, res) => {
    res.status(200).render('login', {
        title: "Login"
    })
});


// exports.getAddCategoryPage = catchAsync(async (req, res, next) => {
//
//     const categories = await Category.find({soft_delete: 0});
//
//
//     res.status(200).render('admin/admin_add_category', {
//         title: "add category",
//         category: categories
//     })
// })
//
// exports.getAddProductPage = catchAsync(async (req, res, next) => {
//
//     const category = await Category.find({soft_delete: 0});
//     const newProduct = await Product.find();
//     res.status(200).render('admin/admin_add_product', {
//         title: "Add product",
//         category: category,
//         product: newProduct
//     })
// });


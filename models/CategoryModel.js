const crypto = require('crypto');

const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({

    name: {
        type: String,
        unique: true,
        required: [true, "Category must have a name!"],
        minlength: 3,
        maxlength: 20,
        trim: true
    },
    slug: {
        type: String
    },
    soft_delete: {
        type: Boolean,
        default:0
    }



});

const Category = mongoose.model('Category', categorySchema);

module.exports = Category;


const crypto = require('crypto');
const mongoose = require('mongoose');
const Category = require('./CategoryModel');

const productSchema = new mongoose.Schema({

    name: {
        type: String,
        required: [true, "Category must have a name!"],
        minlength: 4,
        maxlength: 20,
        trim: true,

    },
    slug: {
        type: String
    },

    // category: {
    //     type: String,
    //     required: ['true', "Choose product category!"]

    //},
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: ['true', "Choose product category!"]},

    price: {
      type: Number,
        required: [true, "A product must have a name!"]
    },

    priceDiscount: {
        type: Number,
        validate: {
            validator: function (value) {
            return value < this.price
            },
            message: 'Discount price ({VALUE}) must be less than price)'
            //TAKES value from user input
        }
    },

    summary: {
        type: String,
        trim: true,
        //required: [true, 'A product must have a description!']
    },
    description: {
        type: String,
        trim: true
    },

    imageCover: {
        type: String,
        required: [true, 'A tour must have cover image!'],
    },
    images: [String],
    createdAt: {
        type: Date,
        default: Date.now(),
        select: false

    },
    soft_delete: {
        type: Boolean,
        default:0
    }
},{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
});



const Product = mongoose.model('Product', productSchema);

module.exports = Product;


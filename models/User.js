const crypto = require('crypto');
const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'User must have a name'],
        minlength: 4,
        maxlength: 20,
        trim: true
    },
    email: {
        unique: true,
        type: String,
        required: [true, 'Please provide your e-mail'],
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email']
    },
    photo: String,
    role: {
        type: String,
        enum: ['user', 'guide', 'lead-guide', 'admin'],
        default: 'user'
    },

    password: {
        type: String,
        required: [true, 'Please provide a password'],
        minlength: 8,
        select: false // it will automatically will never show in any output
    },

    passwordConfirm: {
        type: String,
        required: [true, 'Please confirm your password'],
        validate: {
            //this only works on CREATE and SAVE!!!
            validator: function (el) {
                return el === this.password // abc === abc (returns true or false)
            },
            message: "Passwords are not the same!"
        }
    },
    passwordChangedAt: Date,
    passwordResetToken: String,
    passwordResetExpires: Date,
    active: {
        type: Boolean,
        default: true,
        select: false
    }
});

UserSchema.pre('save', async function (next) {
    // Only run this function if password was actually modified
    if (!this.isModified('password')) return next();

    // hash the password with cost 12
    this.password = await bcrypt.hash(this.password, 12);

    // delete passwordConfirm field
    this.passwordConfirm = undefined;
    next();
});

UserSchema.pre(/^find/, function (next) {
// this points to current query
    this.find({active: {$ne: false}});
    next();
});

UserSchema.pre('save', function (next) {
    if(!this.isModified('password') || this.isNew) return next();
    // saving to db takes some time, so we have decrease created time in order to avoid bugs
    this.passwordChangedAt = Date.now()-1000;
    next();
});

// Instance method  // function name
UserSchema.methods.correctPassword = async function (candidatePassword, userPassword) {
    return await bcrypt.compare(candidatePassword, userPassword);
    //returns sample true or false
};

UserSchema.methods.changedPasswordAfter = function (JWTTimestamp) {

    if (this.passwordChangedAt) {
        const changedTimeStamp = parseInt(this.passwordChangedAt.getTime() / 1000, 10);
        return JWTTimestamp < changedTimeStamp;
    }

// False means not changed
    return false;
};

UserSchema.methods.createPasswordResetToken = function () {
    const resetToken = crypto.randomBytes(32).toString('hex');

    this.passwordResetToken = crypto.createHash('sha256')
        .update(resetToken)
        .digest('hex');

    console.log({resetToken}, this.passwordResetToken);

    //10 min in milliseconds
    this.passwordResetExpires = Date.now() + 10 * 60 * 1000;

    return resetToken;

};

const User = mongoose.model('User', UserSchema);

module.exports = User;

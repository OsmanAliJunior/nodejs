const mongoose = require('mongoose');
const slugify = require('slugify');
// const validator = require('validator');
// const User = require('./User');
const Category = require('./CategoryModel');

const toursSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "A tour must have a name"],
        unique: true,
        trim: true,
        maxlength: [40, 'A tour name must have less or equal then 40 characters'],
        minlength: [10, 'A tour name must have less or equal then 10 characters']
        // validate: [validator.isAlpha, 'Tour name must only contain characters']
    },
    slug: {
        type: String
    },
    duration: {
        type: Number,
        required: [true, "A tour must have a duration!"]
    },
    maxGroupSize: {
        type: Number,
        required: [true, 'Tour must have a group size!']
    },
    difficulty: {
        type: String,
        required: [true, 'A tour must have difficulty'],
        enum: {
            values: ['easy', 'medium', 'hard'],
            message: 'Difficulty is either: easy, medium, difficult'
        }
    },
    ratingsAverage: {
        type: Number,
        default: 4.5,
        min: [1, 'Rating must be above 1.0'],
        max: [5, 'Rating must be below 5.0'],
    },
    ratingsQuantity: {
        type: Number,
        default: 0
    },
    price: {
        type: Number,
        required: [true, 'A tour must have a price']
    },
    priceDiscount: {
        type: Number,
        validate: {
            validator: function (value) {
                // this only points to current doc on NEW document creation
                return value < this.price; // 100 < 200
            },
            message: 'Discount price ({VALUE}) must be less than price)'
            //TAKES value from user input
        },
    },
    summary: {
        type: String,
        trim: true,
        required: [true, 'A tour must have a description!']
    },
    description: {
        type: String,
        trim: true
    },
    imageCover: {
        type: String,
        required: [true, 'A tour must have cover image!'],
    },
    images: [String],
    createdAt: {
        type: Date,
        default: Date.now(),
        select: false

    },
    startDates: [Date],
    secretTour: {
        type: Boolean,
        default: false
    },
    startLocation: {
        // GeoJSON in order to specify geoData
        type: {
            type: String,
            default: 'Point',
            enum: ['Point']
        },
        coordinates: [Number],
        address: String,
        description: String
    },
    locations: [
        {
            type: {
                type: String,
                default: 'Point',
                enum: ['Point']
            },
            coordinates: [Number],
            address: String,
            description: String,
            day: Number
        }
    ],
    guides: [
        {
            type: mongoose.Schema.ObjectId,
            ref: 'User'
        }
    ],
}, {
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
});

// improves speed of reading from database
// toursSchema.index({price: 1});
toursSchema.index({price: 1, ratingsAverage: -1});
toursSchema.index({slug: 1});

toursSchema.virtual('durationWeeks').get(function () {
    return this.duration / 7;
});

//virtual populate
toursSchema.virtual('reviews', {
    ref: 'Review',
    foreignField: 'tour',
    localField: '_id'
});

//  DOCUMENT MIDDLEWARE, runs before the    .save()  &  .create()
toursSchema.pre('save', function (next) {
    this.slug = slugify(this.name, {lowercase: true});
    next();
});
// tuns this middleware before save method and takes users from users collections
// toursSchema.pre('save', async function (next) {
//     const guidesPromises = this.guides.map(async id => User.findById(id));
//     this.guides = await Promise.all(guidesPromises);
//     next();
// });


//toursSchema.pre('find', function (next) {
toursSchema.pre(/^find/, function (next) {
    this.find({secretTour: {$ne: true}});
    this.start = Date.now();
    next();
});

// "this" always points to the current query
toursSchema.pre('/^find/', function (next) {
this.populate({
        path: 'guides',
        select: '-__v -passwordChangeAt'
    });
next();
});


/////// AGGREGATION MIDDLEWARE
toursSchema.pre('aggregate', function (next) {
    this.pipeline().unshift({$match: {secretTour: {$ne: true}}});
    console.log(this.pipeline());
    next();
});


toursSchema.post(/^find/, function (docs, next) {
    console.log(`Query took ${Date.now() - this.start} milliseconds`);
    next();
});


const Tour = mongoose.model('Tour', toursSchema);

module.exports = Tour;



const addProduct = async (name, category, price, priceDiscount, description, imageCover, images) => {
        try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:8000/api/v1/admin/add-product',
            data: {
                name: name,
                category: category,
                price: price,
                priceDiscount: priceDiscount,
                summary: description,
                imageCover: imageCover,
                images: images
            }
        });



        if (res.data.status === 'Success') {
            location.reload();
        }

    } catch (e) {
        alert(e)

    }
};

document.querySelector('.form-control').addEventListener('submit', e => {
    e.preventDefault();
    const name = document.getElementById('cat_name').value;
    const category = document.getElementById('category_type').value;
    const price = document.getElementById('price').value;
    const discountPrice = document.getElementById('discount').value;
    const description = document.getElementById('description').value;
    const imageCover = document.getElementById('img_cover').value;
    const image = document.getElementById('images').files;
     //console.log(name, category, price, discountPrice, description, imageCover, images)

const images = [];
for(let i = 0; i < image.length; i++){
    images.push(image[i].name)
}
addProduct(name,category,price,discountPrice, description, imageCover, images)



});


const deleteProducts = async (_id) => {

    try {
        const res = await axios({
            method: 'DELETE',
            url: `http://localhost:8000/api/v1/admin/products/${_id}`,
        });
        location.reload();
    } catch (e) {
        console.log(e.status);
        console.log(e.name)
    }
}
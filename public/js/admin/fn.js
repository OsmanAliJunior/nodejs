const addCategory = async name => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:8000/api/v1/admin/add-category',
            data: {
              name: name
            }
        });

       if (res.data.status === 'Success') {
            location.reload();
        }

    } catch (e) {
        console.log(e.status);
        console.log(e.name)
    }
};

const deleteCategory = async (_id) => {

    try {
        const res = await axios({
            method: 'DELETE',
            url: `http://localhost:8000/api/v1/admin/category/${_id}`,
        });
        location.reload();
} catch (e) {
        console.log(e.status);
        console.log(e.name)
    }
}

document.querySelector('.form1').addEventListener('submit', e => {
    e.preventDefault();
    const name = document.getElementById('cat_name').value;
    addCategory(name)
});

const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if(e.target.closest(selector)){
            handler(e);
        }
    })
}

//
// on(document, 'click', '.button', e => {
//     var id = document.getElementsByClassName('.button');
// alert(`${id}`)
//     // deleteCategory("1231232")
// })

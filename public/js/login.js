const query = document.getElementById('.form');
console.log(query);

const showAlert = (type, msg) => {
    hideAlert();

    const markup = `<div class="alert alert--${type}">${msg}</div>`;
    document.querySelector('body').insertAdjacentHTML(
        'afterbegin',
        markup
    );
    window.setTimeout(hideAlert,5000);
};


 const hideAlert = () => {
    const el = document.querySelector('.alert');
    if(el) {
        el.parentElement.removeChild(el);
    }

};


 const login = async (email, password) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:8000/api/v1/users/login',
            data: {
                email: email,
                password: password
            }
        });
        console.log(res);

        if (res.data.status === 'success') {
        showAlert('success', 'Logged in succesfully');
            // window.setTimeout(() => {
            //     location.assign('/')
            // }, 5000)
            window.location.href="/"
        }

    } catch (e) {
        // showAlert('error', 'Something went wrong!');
        console.log(e.message);
    }
};

(query) ? query.addEventListener('click', e => {
    e.preventDefault();
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    login(email, password);


}): null;




const  query = document.getElementById('logout');


const logout = async () => {
    try {
        const res = await axios({
            method: 'GET',
            url: 'http://localhost:8000/api/v1/users/logout',
        });
        console.log(res);
        location.reload();

    } catch (err) {
        console.log(err.response);
        showAlert('error', 'Error logging out! Try again.');
    }
};



(query) ? query.addEventListener('click', (e)=>{
    e.preventDefault();
    logout();
}) : null;



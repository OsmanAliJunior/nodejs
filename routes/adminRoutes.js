const express = require('express');
const router = express.Router();
const adminController = require('../controllers/admin/adminViewController');
const viewController = require('../controllers/viewController');
const authController = require('../controllers/authController');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});


// init upload
const upload = multer({
    storage: storage
}).single('images');



//router.use(authController.isLoggedIn);

router
    .route('/add-product')
    .get(adminController.getAddProductPage)
    .post(adminController.AddProduct, adminController.resizeImages)


router
    .route('/add-category')
    .get(adminController.getAddCategoryPage)
    .post(adminController.AddCategory)




//deleting category
router
    .route('/category/:id')
    .delete(authController.protectedAuthRoute, authController.restrictTo('admin'),
        adminController.deleteCategory);

//deleting products
router
    .route('/products/:id')
    .delete(authController.protectedAuthRoute, authController.restrictTo('admin'), adminController.deleteProducts)


router.route('/upload').post((req, res) => {
    upload(req, res, (err) => {
        if(err){
            res.render('error occured')
        }else {
            alert('test');
            res.send('test')
        }
    })
});


module.exports = router;
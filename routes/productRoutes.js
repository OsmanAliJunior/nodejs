const express = require('express');
const router = express.Router();

const productController = require('../controllers/productController');
const authController = require('../controllers/authController');
const reviewRouter = require('../routes/reviewRoutes');


// router
//     .route('/:tourId/reviews')
//     .post(
//         authController.protectedAuthRoute,
//         authController.restrictTo('user'),
//     reviewController.creatReview);

// router.use('/:tourId/reviews', reviewRouter);


// router
//     .route('/top-5-cheap')
//     .get(productController.aliasTopTours, productController.getAllProducts);

// router
//     .route('/tour-stats')
//     .get(productController.getTourStats);

// router
//     .route('/monthly-plan/:year')
//     .get(
//         authController.protectedAuthRoute,
//         authController.restrictTo('admin', 'lead-guide', 'guide'),
//         productController.getMonthlyPlan
//    );

// router
//     .route('/')
//     .get(productController.getAllProducts)
//     .post(authController.protectedAuthRoute,
//         authController.restrictTo('admin', 'lead-guide'),
//         productController.createTour);


// router
//     .route('/:id')
//     .get(productController.getTour)
//     .patch(
//         authController.protectedAuthRoute,
//         authController.restrictTo('admin', 'lead-guide'),
//         productController.updateTour
//     )
//     .delete(
//         authController.protectedAuthRoute,
//         authController.restrictTo('admin', 'lead-guide'),
//         productController.deleteTour
//     );

//deleting category
// router
//     .route('/admin/category/:id')
//     .delete(authController.protectedAuthRoute, authController.restrictTo('admin'),
//         productController.deleteCategory);
//
// //deleting products
// router
//     .route('/admin/products/:id')
//     .delete(authController.protectedAuthRoute, authController.restrictTo('admin'), productController.deleteProducts)


module.exports = router;

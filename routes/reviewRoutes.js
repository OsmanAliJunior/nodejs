const express = require('express');


const reviewController = require('../controllers/reviewController');
const authController = require('../controllers/authController');

const router = express.Router({mergeParams: true});

router.use(authController.protectedAuthRoute);
router
    .route('/')
    .get(reviewController.getAllReview)
    .post(
        authController.restrictTo('user'),
        reviewController.setTourUserIdx,
        reviewController.createReview
    );

router
    .route('/:id')
    .get(reviewController.getAllReview)
    .patch(
        authController.restrictTo('user', 'admin'),
        reviewController.updateReview)
    .delete(
        authController.restrictTo('user', 'admin'),
        reviewController.deleteReview);


module.exports = router;

const express = require('express');
const viewController = require('../controllers/viewController');
const authController = require('../controllers/authController');
const adminController = require('../controllers/admin/adminViewController');

const router = express.Router();



//all routes that user can access after login, must be after isLogged in function
router.use(authController.isLoggedIn);

router.get('/', viewController.getIndexPage);
router.get('/tour/:slug', authController.protectedAuthRoute,  viewController.getTour);
router.get('/login', viewController.getLoginForm);
router.get('/logout', viewController.getLoginForm);
 router.get('/admin/add-category', adminController.getAddCategoryPage)
 router.get('/admin/add-product', adminController.getAddProductPage)


// authController.protectedAuthRoute,

module.exports = router;

 const nodemailer = require('nodemailer');


const sendEmail = async options => {

    // 1) Create a transporter

    // hint Activate in mail "less secure app" option
    // this was for gmail as example
    // const transporter = nodemailer.createTransport({
    //     service: 'Gmail',
    //     auth: {
    //         user: process.env.EMAIL_USERNAME,
    //         password: process.env.EMAIL_PASSWORD
    //     }
    //      })

    const transporter = nodemailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        auth: {
            user: process.env.EMAIL_USERNAME,
            pass: process.env.EMAIL_PASSWORD
        }
    });






    // 2) Define the email options

    const mailOptions = {
        from: '"Hello hello" <odiyar@mail.ru>',
        to: options.email,
        subject: options.subject,
        text: options.message
        //html:
    };

    // 3) Actually send the mail
    await transporter.sendMail(mailOptions); //return a promise

};

module.exports = sendEmail;


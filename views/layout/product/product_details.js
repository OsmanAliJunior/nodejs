// {{> head }}
// <title>{{title}}</title>
// </head>
// <body>
// {{> navbar}}
//
// <section class="section-header">
//     <div class="heading-box">
//     <h1 class="heading-primary">
//     <span>{{tour.name}}</span
// >
// </h1>
// <div class="heading-box__group">
//     <div class="heading-box__detail">
//     <svg class="heading-box__icon">
//     <use xlink:href="/img/icons.svg#icon-clock"></use>
//     </svg>
//     <span style="color: black" class="heading-box__text">{{tour.duration}} days</span>
// </div>
// <div class="heading-box__detail">
//     <svg class="heading-box__icon">
//     <use xlink:href="/img/icons.svg#icon-map-pin"></use>
//     </svg>
//     <span style="color: black" class="heading-box__text">{{tour.startLocation.description}}</span>
// </div>
// </div>
// </div>
// </section>
//
// <section class="section-description">
//     <div class="overview-box">
//     <div>
//     <div class="overview-box__group">
//     <h2 class="heading-secondary ma-bt-lg">Quick facts</h2>
// <div class="overview-box__detail">
//     <svg class="overview-box__icon">
//     <use xlink:href="/img/icons.svg#icon-calendar"></use>
//     </svg>
//     <span class="overview-box__label">Next date</span>
// <span class="overview-box__text">{{toLocaleString tour.startDates.[1] }}</span>
// </div>
// <div class="overview-box__detail">
//     <svg class="overview-box__icon">
//     <use xlink:href="/img/icons.svg#icon-trending-up"></use>
//     </svg>
//     <span class="overview-box__label">Difficulty</span>
//     <span class="overview-box__text">{{tour.difficulty}}</span>
// </div>
// <div class="overview-box__detail">
//     <svg class="overview-box__icon">
//     <use xlink:href="/img/icons.svg#icon-user"></use>
//     </svg>
//     <span class="overview-box__label">Participants</span>
//     <span class="overview-box__text">{{tour.maxGroupSize}} people</span>
// </div>
// <div class="overview-box__detail">
//     <svg class="overview-box__icon">
//     <use xlink:href="/img/icons.svg#icon-star"></use>
//     </svg>
//     <span class="overview-box__label">Rating</span>
//     <span class="overview-box__text">{{tour.ratingsAverage}} / 5</span>
// </div>
// </div>
//
// <div class="overview-box__group">
//     <h2 class="heading-secondary ma-bt-lg">Your tour guides</h2>
//
// {{#each tour.guides}}
// <div class="overview-box__detail">
//     <img
// src="/img/users/{{this.photo}}"
// alt="Lead guide"
// class="overview-box__img"
//     />
//     <span class="overview-box__label">{{this.role}}</span>
// <span class="overview-box__text">{{this.name}}</span>
// </div>
// {{/each}}
//
// </div>
// </div>
// </div>
//
// <div class="description-box">
//     <h2 class="heading-secondary ma-bt-lg">About {{tour.name}} tour</h2>
// <p class="description__text">
//     {{  tour.description }}
// </p>
//     <p class="description__text">
//         {{split tour.description.[1]}}
// </p>
//     </div>
//     </section>
//
//     <section class="section-pictures">
//         {{#each tour.pictures}}
// <div class="picture-box">
//         <img
// class="picture-box__img picture-box__img--1"
//     src="/img/{{this.img}}"
//     alt="The Park Camper Tour 1"
//         />
//         </div>
//     {{/each }}
//     </section>
//
//     <section class="section-map">
//         <div id="map"></div>
//     <script>
//     mapboxgl.accessToken =
//         'pk.eyJ1Ijoiam9uYXNzY2htZWR0bWFubiIsImEiOiJjam54ZmM5N3gwNjAzM3dtZDNxYTVlMnd2In0.ytpI7V7w7cyT1Kq5rT9Z1A';
//
//         const geojson = {
//             type: 'FeatureCollection',
//             features: [
//                 {
//                     type: 'Feature',
//                     geometry: {
//                         type: 'Point',
//                         coordinates: [-112.987418, 37.198125]
//                     },
//                     properties: {
//                         description: 'Zion Canyon National Park'
//                     }
//                 },
//                 {
//                     type: 'Feature',
//                     geometry: {
//                         type: 'Point',
//                         coordinates: [-111.376161, 36.86438]
//                     },
//                     properties: {
//                         description: 'Antelope Canyon'
//                     }
//                 },
//                 {
//                     type: 'Feature',
//                     geometry: {
//                         type: 'Point',
//                         coordinates: [-112.115763, 36.058973]
//                     },
//                     properties: {
//                         description: 'Grand Canyon National Park'
//                     }
//                 },
//                 {
//                     type: 'Feature',
//                     geometry: {
//                         type: 'Point',
//                         coordinates: [-116.107963, 34.011646]
//                     },
//                     properties: {
//                         description: 'Joshua Tree National Park'
//                     }
//                 }
//             ]
//         };
//
//         const map = new mapboxgl.Map({
//             container: 'map',
//             style: 'mapbox://styles/jonasschmedtmann/cjnxfn3zk7bj52rpegdltx58h',
//             scrollZoom: false
//         });
//
//         const bounds = new mapboxgl.LngLatBounds();
//
//         geojson.features.forEach(function (marker) {
//             var el = document.createElement('div');
//             el.className = 'marker';
//
//             new mapboxgl.Marker({
//                 element: el,
//                 anchor: 'bottom'
//             })
//                 .setLngLat(marker.geometry.coordinates)
//                 .addTo(map);
//
//             new mapboxgl.Popup({
//                 offset: 30,
//                 closeOnClick: false
//             })
//                 .setLngLat(marker.geometry.coordinates)
//                 .setHTML('<p>' + marker.properties.description + '</p>')
//                 .addTo(map);
//
//             bounds.extend(marker.geometry.coordinates);
//         });
//
//         map.fitBounds(bounds, {
//             padding: {
//                 top: 200,
//                 bottom: 150,
//                 left: 50,
//                 right: 50
//             }
//         });
//
//         map.on('load', function () {
//             map.addLayer({
//                 id: 'route',
//                 type: 'line',
//                 source: {
//                     type: 'geojson',
//                     data: {
//                         type: 'Feature',
//                         properties: {},
//                         geometry: {
//                             type: 'LineString',
//                             coordinates: [
//                                 [-112.987418, 37.198125],
//                                 [-111.376161, 36.86438],
//                                 [-112.115763, 36.058973],
//                                 [-116.107963, 34.011646]
//                             ]
//                         }
//                     }
//                 },
//                 layout: {
//                     'line-join': 'round',
//                     'line-cap': 'round'
//                 },
//                 paint: {
//                     'line-color': '#55c57a',
//                     'line-opacity': 0.6,
//                     'line-width': 3
//                 }
//             });
//         });
//     </script>
//         </section>
//
//         <section class="section-reviews">
//             <div class="reviews">
//             {{#each reviews}}
//
//     <div class="reviews__card">
//             <div class="reviews__avatar">
//             <img
//         src="/img/users/{{this.user.photo}}"
//         alt="Jim Brown"
//     class="reviews__avatar-img"
//             />
//             <h6 class="reviews__user">
//             {{this.user}}
//     </h6>
//         </div>
//         <p class="reviews__text">
//             {{this.review}}
//     </p>
//         <div class="reviews__rating">
//             {{#times 5}}
//     <svg class="reviews__star reviews__star--active">
//             <use xlink:href="/img/icons.svg#icon-star"></use>
//             </svg>
//         {{/times}}
//         </div>
//         </div>
//             {{/each}}
//             </div>
//             </section>
//
//
//             <section class="section-cta">
//                 <div class="cta">
//                 <div class="cta__img cta__img--logo">
//                 <img src="img/logo-white.png" alt="Natours logo" class=""/>
//                 </div>
//                 <img src="img/tour-5-2.jpg" alt="" class="cta__img cta__img--1"/>
//                 <img src="img/tour-5-1.jpg" alt="" class="cta__img cta__img--2"/>
//
//                 <div class="cta__content">
//                 <h2 class="heading-secondary">What are you waiting for?</h2>
//             <p class="cta__text">
//                 10 days. 1 adventure. Infinite memories. Make it yours today!
//             </p>
//             <button class="btn btn--green span-all-rows">Book tour now!</button>
//             </div>
//             </div>
//             </section>
//
//
//
//                 {{> end }}
